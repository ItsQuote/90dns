>nul chcp 65001
@echo off
ECHO ╔══════════════════════════════════════════════════╗
ECHO ║ This file should be run from the command line of ║
ECHO ║ a Windows DNS server. You do not run it from the ║
ECHO ║ Switch.                                          ║
ECHO ║                                                  ║
ECHO ║  !!IF YOU DO NOT RUN A DNS SERVER THIS IS THE!!  ║
ECHO ║                !!WRONG FILE!!                    ║
ECHO ╚══════════════════════════════════════════════════╝
ECHO.
ECHO Choose an option:
ECHO.
ECHO b) Block connections to Nintendo servers.
ECHO u) Unblock connections to Nintendo servers.
ECHO q) Quit
ECHO.
ECHO [b,u,Q]

choice /n /c:buq /t 60 /d q

IF %ERRORLEVEL% EQU 1 GOTO SETDNS
IF %ERRORLEVEL% EQU 2 GOTO UNSETDNS
IF %ERRORLEVEL% EQU 3 GOTO NOCHANGES


:SETDNS
dnscmd /recordadd ctest.cdn.nintendo.net A 1.1.1.1
dnscmd /recordadd conntest.nintendowifi.net A 1.1.1.1
dnscmd /recordadd nintendo.com A 0.0.0.0
dnscmd /recordadd nintendo.co.uk A 0.0.0.0
dnscmd /recordadd nintendo-europe.com A 0.0.0.0
dnscmd /recordadd nintendo.jp A 0.0.0.0
dnscmd /recordadd nintendo.co.jp A 0.0.0.0
dnscmd /recordadd nintendo.es A 0.0.0.0
dnscmd /recordadd nintendo.co.kr A 0.0.0.0
dnscmd /recordadd nintendo.tw A 0.0.0.0
dnscmd /recordadd nintendo.com.hk A 0.0.0.0
dnscmd /recordadd nintendo.com.au A 0.0.0.0
dnscmd /recordadd nintendo.co.nz A 0.0.0.0
dnscmd /recordadd nintendo.at A 0.0.0.0
dnscmd /recordadd nintendo.be A 0.0.0.0
dnscmd /recordadd nintendods.cz A 0.0.0.0
dnscmd /recordadd nintendo.dk A 0.0.0.0
dnscmd /recordadd nintendo.de A 0.0.0.0
dnscmd /recordadd nintendo.fi A 0.0.0.0
dnscmd /recordadd nintendo.fr A 0.0.0.0
dnscmd /recordadd nintendo.gr A 0.0.0.0
dnscmd /recordadd nintendo.hu A 0.0.0.0
dnscmd /recordadd nintendo.it A 0.0.0.0
dnscmd /recordadd nintendo.nl A 0.0.0.0
dnscmd /recordadd nintendo.no A 0.0.0.0
dnscmd /recordadd nintendo.pt A 0.0.0.0
dnscmd /recordadd nintendo.ru A 0.0.0.0
dnscmd /recordadd nintendo.co.za  A 0.0.0.0
dnscmd /recordadd nintendo.se A 0.0.0.0
dnscmd /recordadd nintendo.ch  A 0.0.0.0
dnscmd /recordadd google-analytics.com A 0.0.0.0
dnscmd /recordadd googletagmanager.com A 0.0.0.0

ECHO.
ECHO DNS Changes were added.
ECHO.
GOTO END

:UNSETDNS
dnscmd /recorddelete ctest.cdn.nintendo.net A 1.1.1.1
dnscmd /recorddelete conntest.nintendowifi.net A 1.1.1.1
dnscmd /recorddelete nintendo.com A 0.0.0.0
dnscmd /recorddelete nintendo.co.uk A 0.0.0.0
dnscmd /recorddelete nintendo-europe.com A 0.0.0.0
dnscmd /recorddelete nintendo.jp A 0.0.0.0
dnscmd /recorddelete nintendo.co.jp A 0.0.0.0
dnscmd /recorddelete nintendo.es A 0.0.0.0
dnscmd /recorddelete nintendo.co.kr A 0.0.0.0
dnscmd /recorddelete nintendo.tw A 0.0.0.0
dnscmd /recorddelete nintendo.com.hk A 0.0.0.0
dnscmd /recorddelete nintendo.com.au A 0.0.0.0
dnscmd /recorddelete nintendo.co.nz A 0.0.0.0
dnscmd /recorddelete nintendo.at A 0.0.0.0
dnscmd /recorddelete nintendo.be A 0.0.0.0
dnscmd /recorddelete nintendods.cz A 0.0.0.0
dnscmd /recorddelete nintendo.dk A 0.0.0.0
dnscmd /recorddelete nintendo.de A 0.0.0.0
dnscmd /recorddelete nintendo.fi A 0.0.0.0
dnscmd /recorddelete nintendo.fr A 0.0.0.0
dnscmd /recorddelete nintendo.gr A 0.0.0.0
dnscmd /recorddelete nintendo.hu A 0.0.0.0
dnscmd /recorddelete nintendo.it A 0.0.0.0
dnscmd /recorddelete nintendo.nl A 0.0.0.0
dnscmd /recorddelete nintendo.no A 0.0.0.0
dnscmd /recorddelete nintendo.pt A 0.0.0.0
dnscmd /recorddelete nintendo.ru A 0.0.0.0
dnscmd /recorddelete nintendo.co.za A 0.0.0.0
dnscmd /recorddelete nintendo.se A 0.0.0.0
dnscmd /recorddelete nintendo.ch A 0.0.0.0
dnscmd /recorddelete nintendo.pl A 0.0.0.0
dnscmd /recorddelete nintendoswitch.cn A 0.0.0.0
dnscmd /recorddelete nintendoswitch.com.cn A 0.0.0.0
dnscmd /recorddelete nintendoswitch.com A 0.0.0.0
dnscmd /recorddelete google-analytics.com A 0.0.0.0
dnscmd /recorddelete googletagmanager.com A 0.0.0.0

ECHO.
ECHO DNS Changes were removed.
ECHO.
GOTO END

:NOCHANGES
ECHO.
ECHO No changes were made.
ECHO.

:END

pause
