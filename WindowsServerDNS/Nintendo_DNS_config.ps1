﻿
Write-Host -ForegroundColor DarkGreen ' ╔══════════════════════════════════════════════════╗'
Write-Host -ForegroundColor DarkGreen ' ║' -NoNewline; Write-Host ' This file should be run from the command line of ' -NoNewline; Write-Host -ForegroundColor DarkGreen '║'
Write-Host -ForegroundColor DarkGreen ' ║' -NoNewline; Write-Host ' a Windows DNS server. You do not run it from the ' -NoNewline; Write-Host -ForegroundColor DarkGreen '║'
Write-Host -ForegroundColor DarkGreen ' ║' -NoNewline; Write-Host ' Switch.                                          ' -NoNewline; Write-Host -ForegroundColor DarkGreen '║'
Write-Host -ForegroundColor DarkGreen ' ║                                                  ║'
Write-Host -ForegroundColor DarkGreen ' ║' -NoNewline; Write-Host -ForegroundColor Red '  !!IF YOU DO NOT RUN A DNS SERVER THIS IS THE!!  ' -NoNewline; Write-Host -ForegroundColor DarkGreen '║'
Write-Host -ForegroundColor DarkGreen ' ║' -NoNewline; Write-Host -ForegroundColor Red '                !!WRONG FILE!!                    ' -NoNewline; Write-Host -ForegroundColor DarkGreen '║'
Write-Host -ForegroundColor DarkGreen ' ╚══════════════════════════════════════════════════╝'
Write-Host -ForegroundColor DarkGreen ''
Write-Host ' Choose an option:'
Write-Host ''
Write-Host ' b) Block connections to Nintendo servers.'
Write-Host ' u) Unblock connections to Nintendo servers.'
Write-Host ' q) Quit'
Write-Host ''

$option = Read-Host -Prompt ' [b,u,q]'
$zoneName = Read-Host -Prompt 'Enter the Zone Name'

if($option -eq 'b')
{

    Add-DnsServerResourceRecordA -Name "ctest.cdn.nintendo.net" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "1.1.1.1"
    Add-DnsServerResourceRecordA -Name "conntest.nintendowifi.net" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "1.1.1.1"
    Add-DnsServerResourceRecordA -Name "nintendo.com" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.co.uk" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo-europe.com" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.jp" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.co.jp" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.es" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.co.kr" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.tw" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.com.hk" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.com.au" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.co.nz" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.at" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.be" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendods.cz" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.dk" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.de" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.fi" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.gr" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.hu" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.it" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.nl" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.no" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.pt" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.ru" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.co.za" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.se" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.ch" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    Add-DnsServerResourceRecordA -Name "nintendo.pl" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    #Add-DnsServerResourceRecordA -Name "google-analytics.com" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"
    #Add-DnsServerResourceRecordA -Name "googletagmanager.com" -ZoneName $zoneName -AllowUpdateAny -IPv4Address "0.0.0.0"

    Write-Host -ForegroundColor Green 'DNS Changes were added!'
}
elseif($option -eq 'u')
{

    Remove-DnsServerResourceRecord -Name "ctest.cdn.nintendo.net" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "conntest.nintendowifi.net" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.com" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.co.uk" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo-europe.com" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.jp" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.co.jp" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.es" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.co.kr" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.tw" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.com.hk" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.com.au" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.co.nz" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.at" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.be" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendods.cz" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.dk" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.de" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.fi" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.gr" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.hu" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.it" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.nl" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.no" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.pt" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.ru" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.co.za" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.se" -ZoneName $zoneName -RRType "A" -Force
    Remove-DnsServerResourceRecord -Name "nintendo.ch" -ZoneName $zoneName -RRType "A" -Force
    #Remove-DnsServerResourceRecord -Name "google-analytics.com" -ZoneName $zoneName -RRType "A" -Force
    #Remove-DnsServerResourceRecord -Name "googletagmanager.com" -ZoneName $zoneName -RRType "A" -Force

    Write-Host -ForegroundColor Green 'DNS Changes were removed!'
}

